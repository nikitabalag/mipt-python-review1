import re
import codecs
from collections import defaultdict
import random
import argparse
import pickle
import time


def make_sen_with_word(model, length, word):
    if word:
        buf = 0
        phrase = ''
        t0 = word
        phrase += t0
        t1 = choose_next_word(model[t0])
        while buf < length:  # пока строка не слишком длинная
            t0, t1 = t1, choose_next_word(model[t1])
            if not (t0 in ";:.?..!.." or t0 == '$'):
                phrase += ' ' + t0
                buf += 1
        return phrase.capitalize()
    else:  # если не заданно первое слово
        buf = 0
        phrase = ''
        t0, t1 = '$', random.choice(list(model.keys()))  #  выберем случайно
        while buf < length:
            t0, t1 = t1, choose_next_word(model[t1])
            if t1 in ".!?,;:" or t0 == '$':
                phrase += t1
                buf += 1
            else:
                buf += 1
                phrase += ' ' + t1
        return phrase.capitalize()


def choose_next_word(inner_dict):
    buf_list = []
    for word, freq in inner_dict.items():
        for _ in range(freq):
            buf_list.append(word)
    return random.choice(buf_list)


def take_model(corpus):
    f = codecs.open(corpus, mode='rb')
    model = pickle.load(f)
    f.close()
    return model


def main():
    parser = argparse.ArgumentParser(description='Generate '
                                                 'text on given model')
    parser.add_argument('--output-dir', '-o',
                        help='choose output directory')
    parser.add_argument('--model', '-m', required=True,
                        help='choose file where model')
    parser.add_argument('--length', '-l', type=int, required=True,
                        help='set length of generating text')
    parser.add_argument('--seed', '-s', type=str, help='set word to start')
    args = parser.parse_args()
    model = take_model(args.model)
    if not args.output_dir:
        print(make_sen_with_word(model, args.length, args.seed))
    else:
        f = codecs.open(args.output_dir, mode='w', encoding='UTF-8')
        f.write(make_sen_with_word(model, args.length, args.seed))

if __name__ == "__main__":
    main()
