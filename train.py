import re
import codecs
from collections import defaultdict, Counter
import random
import argparse
import pickle
import os
import sys


def gen_lines(dir, _lower):
    if dir:
        for file in os.listdir(dir):  # выберем txt-шники
            if file.endswith(".txt"):
                data = codecs.open(file, encoding="utf-8")
                if (_lower):  # вынес из-за цикла, чтобы проверить один раз
                    for line in data:
                        yield line.lower()
                else:
                    for line in data:
                        yield line
                data.close()
    else:
        if(_lower):
            for line in sys.stdin:
                yield line.lower()
        else:
            for line in sys.stdin:
                yield line


def gen_tokens(lines, alphabet):
    for line in lines:
        for token in alphabet.findall(line):
            yield token


def gen_bigrams(tokens):
    t0 = ''
    for t1 in tokens:
        yield t0, t1
        t0 = t1


def make_statistic(bigrams):
    bi = defaultdict(int)
    for t0, t1 in bigrams:
        bi[t0, t1] += 1  # смотрим количество повторов каждой пары
    model = defaultdict(dict)
    for (t0, t1), freq in bi.items():
        model[t0].update({t1: freq})

    return model


def record_model(model, dir):
    f = codecs.open(dir, mode='wb')
    pickle.dump(model, f)
    f.close()


def main():
    parser = argparse.ArgumentParser(description='Train model '
                                                 'on given text collection')
    parser.add_argument('--input-dir', '-i',
                        help='choose directory of file to train')
    parser.add_argument('--model', required=True,
                        help='directory of file to save model')
    parser.add_argument('--lc', action='store_true',
                        help='convert to lowercase')
    args = parser.parse_args()
    r_alphabet = re.compile('[а-яА-Я0-9-ёЁ-]+|[:;.?!]+')
    lines = gen_lines(args.input_dir, args.lc)
    tokens = gen_tokens(lines, r_alphabet)
    bigrams = gen_bigrams(tokens)
    model = make_statistic(bigrams)
    record_model(model, args.model)

if __name__ == "__main__":
    main()
